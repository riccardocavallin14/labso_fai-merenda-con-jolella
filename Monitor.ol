/*
Fai Merenda con Jolella File Sharing P2P Netowork
Copyright (C) 2019 
<riccardo.cavallin@studio.unibo.it>
<emanuele.fazzini@studio.unibo.it>
<martino.simonetti@studio.unibo.it>
<michele.bonini2@studio.unibo.it>
<davide.cavallini4@studio.unibo.it>
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include "interface.iol"
include "console.iol"

inputPort MyInput {
    Location: "socket://localhost:8001"
    Protocol: http
    Interfaces: MyInterface
}

init {
  println@Console( "Monitor avviato\n" )();
  global.count = 0
}

execution{ concurrent }

main {

	[ display( message ) ] {
      synchronized( countToken ){
        global.count++;
        println@Console(global.count + ". " + message)()
      }
    }
}
