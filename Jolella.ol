/*
Fai Merenda con Jolella File Sharing P2P Netowork
Copyright (C) 2019 
<riccardo.cavallin@studio.unibo.it>
<emanuele.fazzini@studio.unibo.it>
<martino.simonetti@studio.unibo.it>
<michele.bonini2@studio.unibo.it>
<davide.cavallini4@studio.unibo.it>
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include "interface.iol"
include "console.iol"
include "time.iol"

inputPort MyInput {
  Location: "socket://localhost:8000/"
  Protocol: http
  Interfaces: MyInterface
}

outputPort Monitor {
  Location: "socket://localhost:8001/"
  Protocol: http
  Interfaces: MyInterface
}

init {
  //nel vettore a c'è -1 se la posizione è presente un elemento online
  // 0 se la posizione è libera
  // un valore double che indica il momento in cui si è messo offline
  global.a[0] = 0//; // vettore degli utenti
}

define doJoin {
  synchronized( token ) {
    // se è la prima volta che si collega alla rete
    if (enter.id==-1) {
      i=0;
      id = -1;
      while(i<#global.a) {
        if (global.a[i] == 0) {
          id = i;
          global.a[id] = -1;
          // salvo la input port del jeer nel campo address corrispondente nel vettore dei jeer
          global.a[id].address = enter.address;
          i = #global.a + 1;
          display@Monitor("Il jeer " + enter.address + " si è unito alla rete")
        };
        i++
      };
    // se l'id è ancora -1 significa che il vettore è pieno e che devo aggiungerlo in coda
      if (id == -1) {
        id = i;
        global.a[i] = -1;
        // salvo la input port del jeer nel campo address corrispondente nel vettore dei jeers
        global.a[i].address = enter.address;
        display@Monitor("Il jeer " + enter.address + " si è unito alla rete")
      }
    }
  // non è la prima volta che mi connetto alla rete ed enter.id è l'id della precedente connessione
    else {
      getCurrentTimeMillis@Time()(ms);
      tempo = ms;
      // 10 secondi di tempo
      if (tempo-global.a[enter.id] < 5000) {
        id = -1;
        display@Monitor("Nuova connessione rifiutata per il jeer " + enter.address)
      }
      else {
        global.a[enter.id] = -1;
        id = enter.id;
        display@Monitor("Il jeer " + enter.address + " è tornato online")
      }
    }
  }
}

define doLeave {
  synchronized( token ) {
    getCurrentTimeMillis@Time()(ms);
    // aggiorno il contenuto della cella al momento in cui il jeer lascia la rete
    global.a[id] = ms;
    display@Monitor("Il jeer " + global.a[id].address + " è offline")
  }
}

define doDisconnect {
  synchronized( tokenFile ){
    display@Monitor("Il jeer " + global.a[id].address + " si è disconnesso dalla rete. Tutti i suoi file non sono più disponibili");
    // libero la posizione nel vettore
    global.a[id] = 0;
    // calcello il suo indirizzo
    undef(global.a[id].address);
    // cancello i file dal vettore a
    undef(global.a[id].files)
  }
}

define doShowAllFiles {
	synchronized( tokenFile ){
		for (i=0, i<#global.database, i++) {
			lista.x[i] = global.database[i]
		}		
	}
}

// stampa a video gli identificativi e indirizzi di tutti gli utenti sulla rete
define showAllSeeders {
  synchronized( tokenFile ){
    for (i=0, i<#global.a, i++) {
      println@Console( "id: " + i + "  address: " + global.a[i].address)()
    }
  }
}

define doAddFile {
	synchronized( tokenFile ) {
    	// INSERIMENTO NEL DATABASE
    	// I primi casi sono relativi alla situazione in cui il file è già persente nel database 	  
    	// mentre il terzo ad un nuovo inserimento
		j=0;
		inserito = false;
  	 	// scorre tutto il vettore database
		while (j<#global.database) {
     	// scorro cercando se lo ha già qualcuno
			if (global.database[j] == x.item) {
				k = 0;
     		  	// scorro tutta la lista degli utenti che lo posseggono
       			// esco se ho scorso tutto o se l'ho inserito
				while (k<#global.database[j].users || inserito) {
         			// se l'utente è uguale inserisco sovrascrivo l'id
					if (global.database[j].users[k] == x.id) {
						global.database[j].users[k] = x.id;
            			// inserisce l'elemento nell'ultima posizione della lista files innestata nella posizione x.id di a
           				global.a[x.id].files[#global.a[x.id].files] = x.item;
						inserito = true;
						display@Monitor("Il jeer " + global.a[x.id].address + " ha caricato il file " + x.item)
					};
					k++
				};
       			// se non è lo stesso utente che cerca di ricaricare il file
				if (k == #global.database[j].users) {
        		  	// lasciando k lo mette in automatico nella posizione successiva
					global.database[j].users[k] = x.id;
					inserito = true;
					display@Monitor("Il jeer " + global.a[x.id].address + " ha caricato il file " + x.item)
				}
			};
			j++
		};

   		// terzo caso
		if(!inserito) {
			n = #global.database;
			global.database[n] = x.item;
			global.database[n].users[0] = x.id;
			display@Monitor("Il jeer " + global.a[x.id].address + " ha caricato il file " + x.item)
		}
	}

}

define doRemoveFile {
  synchronized( tokenFile ) {
    i = 0;
    while (i<#global.a[x.id].files) {
      if (global.a[x.id].files[i] == x.item) {
        undef(global.a[x.id].files[i]);
        i = i + #global.a[x.id].files
      };
      i++
    };
    k = 0;
    removed = false;
    // scorro il database
    while (k<#global.database) {
        // se il file corrisponde a quello cercato
      if (global.database[k] == x.item) {
      	// se il seeder è unico procedo a rimuovere tutto 
      	if (#global.database[k].users == 1) {
      		// verifico che l'elemento in prima posizione della lista users abbia lo stesso id di chi tenta di rimuoverlo
      		if (global.database[k].users[0] == x.id) {
      			undef(global.database[k]);
      			display@Monitor("File " + x.item + " rimosso dalla rete da parte del jeer " + global.a[x.id].address);
      			display@Monitor("Il file " + x.item + " non è più disponibile nella rete");
      			removed = true
      		}
      		else {
      			removed = false
      		}
      	} else {
      		// se c'è più di un seeder devo rimuovere soltanto quello corretto
      		for (i=0, i<#global.database[k].users, i++) {
      			// controllo che l'utente sia corretto
      			if (global.database[k].users[i] == x.id) {
      				undef(global.database[k].users[i]);
      				display@Monitor("File " + x.item + " rimosso dalla rete da parte del jeer " + global.a[x.id].address);
      				removed = true
      			} else {
      				removed = false
      			}
      		}	
      	}
      };
      k++
    }
  }
}

define doLookUp {
	synchronized( tokenFile ) {
		found = false;
		k = 0;
	  	// fino a quando non viene trovato oppure scorso tutto il vettore non viene trovato
		while (!found && k<#global.database) {
	  		// se il nome corrisponde alla posizione corrente
			if (name == global.database[k]) {
				found = true;
	  			// aggiungo tutti i jeer che lo possiedono in list 
				for (i=0, i<#global.database[k].users, i++) {
					id_jeer = global.database[k].users[i];
          			// verifico che il jeer non sia offline (in quel caso troverei un current time)
					if (global.a[id_jeer] == -1) {
						list.id[i] = id_jeer;
						list.address[i] = global.a[id_jeer].address
          } else {    // il jeer è offline
          	list.id[i] = -2;
          	list.address[i] = global.a[id_jeer].address + " è offline"
          }
      }
  };
  k++
};
	  // se found è false vuol dire che il fle cercato non esiste 
	  // quindi restituisco list con -1 sul campo id
	  if(!found) {
	  	list.id[0] = -1;
	  	list.address[0] = "Error 404"
	  }
	}
}


execution { concurrent }

main {

  [ join(enter)(id) { doJoin } ]
  [ leave(id) ] { doLeave }
  [ disconnect(id) ] { doDisconnect }
  [ addFile(x) ] { doAddFile }
  [ removeFile(x)(removed) { doRemoveFile } ] 
  [ lookup(name)(list) { doLookUp } ]
  [ showAllFiles()(lista) { doShowAllFiles } ]

}