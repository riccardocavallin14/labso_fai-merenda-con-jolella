/*
Fai Merenda con Jolella File Sharing P2P Netowork
Copyright (C) 2019 
<riccardo.cavallin@studio.unibo.it>
<emanuele.fazzini@studio.unibo.it>
<martino.simonetti@studio.unibo.it>
<michele.bonini2@studio.unibo.it>
<davide.cavallini4@studio.unibo.it>
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include "interface.iol"
include "console.iol"
include "time.iol"
include "file.iol"
include "semaphore_utils.iol"
include "exec.iol"
include "runtime.iol"
include "converter.iol"
include "message_digest.iol"
include "string_utils.iol"

constants {
	MY_ADDRESS = "",
	portaOut = ""
}

inputPort MyInput {
	Location: MY_ADDRESS
	Protocol: http
	Interfaces: MyInterface
}

outputPort Jolella {
	Location: "socket://localhost:8000"
	Protocol: http
	Interfaces: MyInterface
}

outputPort Jeer {
	Location: portaOut // è l'indirizzo del jeer a cui devo inviare il file
	Protocol: http
	Interfaces: MyInterface
}

outputPort Monitor {
  Location: "socket://localhost:8001/"
  Protocol: http
  Interfaces: MyInterface
}

init {
	global.vaiPureAvanti=false;
	global.TiConfermoCheSonoUnTre=false;
	global.online=true;
	global.nomeFile="";
	global.TiConfermoCheSonoUnQuattro=false;
	global.forAddFile[0].id = -1;
	global.forAddFile[0].item = "";
	checkRemoved=true;
	checkFileVar=true;
	myInformation.id=-1;
	myInformation.address=MY_ADDRESS;
	var=true; //variabile necessaria per verificare che l'input dato sia effettivamente plausibile : 1,2,3,4... (opzioni iniziali)
	global.pathDirectoryToShare="";
	rightPath=false;
	registerForInput@Console()();
	join@Jolella ( myInformation )(risposta);
	myInformation.id = risposta;
	println@Console( "\nTi sei collegato alla rete Fai Merenda con Jolella!\n")();
	inserisciPath;
	scelte
}

//menu di scelta che viene stampato in console
define scelte {
		println@Console("\nCosa vuoi fare?")();
		println@Console("1 ----> Vai Offline")();
		println@Console("2 ----> Exit")();
		println@Console("3 ----> Cerca file da scaricare")();
		println@Console("4 ----> Aggiorna il path della vecchia cartella")();
		println@Console("5 ----> Rimuovi un file dalla rete")()
}

define inserisciPath {
	rightPath=false;
	println@Console("Inserisci il path della cartella da condividere.")();
	println@Console("Se all'interno del path saranno presenti delle directory quest'ultime verranno compresse al fine di facilitarne la condivisione")();
	println@Console("Questa operazione può richiedere qualche secondo...")();
	in(pathTmp);
	global.pathDirectoryToShare=pathTmp;
	while(rightPath==false){
		println@Console()();
		exists@File(global.pathDirectoryToShare)(pathExists);
		if(!pathExists){
			println@Console("Path errato. Inserisci il path corretto.")();
			in(pathTmp);
			global.pathDirectoryToShare=pathTmp;
			rightPath=false
		} else {
			println@Console( "File resi disponibili: " )();
			reqFiles.directory=global.pathDirectoryToShare;
			list@File(reqFiles)(resFiles);
			for( i=0 , i<#resFiles.result , i++){
				//controllo se ci sono cartella all'interno della directory e nel caso le zippo e il file compresso viene caricato nella rete
				checkIsDir=global.pathDirectoryToShare + "/" + resFiles.result[i];
				isDirectory@File( checkIsDir )( isDir );
				if(isDir){
					execRequest = "tar";
					with( execRequest ){
						.args[0] = "-zcvf";
						.args[1] = resFiles.result[i] + ".tar.gz";
						.args[2] = resFiles.result[i];
						.workingDirectory = global.pathDirectoryToShare;
						.stdOutConsoleEnable = false
					};
					exec@Exec( execRequest )( execResponse )
				}
			};
			list@File(reqFiles)(resFiles);
			//controllo se nella directory ci sono altre cartelle e nel caso non le carico perchè è presente il formato ".targz"
			//oppure i files iniziano con il ".", in quanto sono files del sistema operativo
			k = 0;
			for( i=0 , i<#resFiles.result , i++){
				checkIsDir=global.pathDirectoryToShare + "/" + resFiles.result[i];
				isDirectory@File( checkIsDir )( isDir );
				if(resFiles.result[i]==void){
					println@Console( "la cartella è vuota" )()
				} else {
					if(!isDir){
						reqStartsWith = resFiles.result[i];
						reqStartsWith.prefix = ".";
						startsWith@StringUtils(reqStartsWith)(resStartsWith);
						//non vogliamo file che inizino con il punto in quanto sono file dell'os
						if(!resStartsWith)
						{
							global.forAddFile[k].id=myInformation.id;
							global.forAddFile[k].item=resFiles.result[i];
							println@Console( global.forAddFile[k].item )();
							addFile@Jolella( global.forAddFile[k] );
							k++
						}
					}
				}
			};
			rightPath=true
		}
	}
}


define doMenu0 {
			global.online=false;
			if(scelta == 0){ //join alla rete
				join@Jolella ( myInformation )(risposta);
				if(risposta==-1){
					println@Console("Errore. Connessione rifiutata, riprova tra 5 secondi. ")();
					getCurrentTimeMillis@Time()(ms);
					tempo1 = ms;
					while(bloccoConnessione==true){
					getCurrentTimeMillis@Time()(ms);
					tempo2 = ms;
						if(tempo2-tempo1<5000){
							bloccoConnessione=true
						}else{
							bloccoConnessione=false
						}
					}
				}else{
						myInformation.id = risposta;
						println@Console( "\nTi sei collegato alla rete!")();
						scelte;
						global.online=true
				}
			}
}
define doMenu1 {
			if(scelta == 1){ //leave
				leave@Jolella(myInformation.id);
				println@Console("Sei offline")();
				println@Console("Premi 0 per riconnetterti ")();
				global.online=false
			}
}

define doMenu2{
		if(scelta == 2) { //exit
			for (i=0, i<#global.forAddFile, i++) {
				removeFile@Jolella(global.forAddFile[i])(removed)
			};
			disconnect@Jolella(myInformation.id)
		}
}

define ricercaFile{ //verifico se il file c'è o meno
			//controllo se il file è gia in mio possesso
			reqFiles.directory=global.pathDirectoryToShare;
			list@File(reqFiles)(resFiles);
			for( i=0 , i<#resFiles.result , i++){
				if(global.checkFile==resFiles.result[i]){
					checkFileVar=false
				}
			};
			if(!checkFileVar){
				println@Console( "Hai già questo file!" )();
				global.TiConfermoCheSonoUnTre=false;
				scelte
			}
			else{
				global.vaiPureAvanti=false;
				lookup@Jolella(scelta)(global.list);
				//dovrei ricevere -1 (list.id == -1) se il file non esiste
				if(#global.list.id == 0){
					println@Console("Il jeer che possiede il file è offline")();
					scelte;
					global.TiConfermoCheSonoUnTre=false
				} else if(global.list.id == -1){
					println@Console("Error 404: File Not Found")();
					global.TiConfermoCheSonoUnTre=false;
					scelte
				} else {
					println@Console("Ecco la lista degli utenti che posseggono il file. Scaricherai un pezzo del file da ciascuno di essi")();
					for(i=0,i<#global.list.id,i++) {
						println@Console("Indirizzo Jeer: " + global.list.address[i]) ()
					};
					global.vaiPureAvanti=true
				}
			}
}

define SceltaId {
				cont=0;
				getCurrentTimeMillis@Time()(start);
				println@Console("Download in corso...")();
				
				// controllo presenza di seeder che sono offline
				for (i = 0, i < #global.list.id, i++) {
					if (global.list.id[i] == -2) {
						undef(global.list.id[i]);
						undef(global.list.address[i]);
						i = i - 1
					}
				};

				fileScaricare.tot = #global.list.id;
				for(i = 0, i < #global.list.id, i++){
					Jeer.location = global.list.address[i];
					fileScaricare.nome = global.nomeFile;
					fileScaricare.part = i;
					// download di un pezzetto del file
					download@Jeer(fileScaricare)(contenuto);

					//NON cancellare il commento sottostante
					//contenuto.part potrebbe essere anche i, MA per sicurezza è meglio cosi
					fileArray[contenuto.part] = contenuto.dati
				};
				//RIMETTO INSIEME IL FILE
				fileTot = "";
				for(i = 0, i < #global.list.id, i++){
					fileTot = fileTot + fileArray[i]
				};
				// generazione hash del file ricevuto
					scope( downloadFile ) {
						install ( UnsupportedOperation => println@Console("Il file ricevuto è corrotto. Si prega di ripovare")() );
						install ( IOException => println@Console("Impossibile convertire il file")());
						md5@MessageDigest(fileTot)(hash_new);
						// se i due hash corrispondono allora non è corrotto e si può procedere
						if (hash_new == contenuto.hash) {
							// si converte da base64 a raw per poterlo scrivere
							base64ToRaw@Converter(fileTot)(file.content);
							if (file instanceof IOExceptionType){
								throw( IOException )
							} else {
								file.filename = global.pathDirectoryToShare + "/" + global.nomeFile;
								// si scrive il file in memoria
								writeFile@File(file)(void);
								getCurrentTimeMillis@Time()(end);
								// calcolo del tempo impiegato
								time = (end - start) / 1000;
								println@Console("\n\tFILE SCARICATO CON SUCCESSO!")();
								println@Console("\tTempo impiegato: " + time + " secondi" )();
								// ora il file viene caricato in rete per renerlo disponibile per gli altri
								forAddFile.id = myInformation.id;
								forAddFile.item = global.nomeFile;
								addFile@Jolella(forAddFile);
								println@Console( "\tOra sei anche tu un seeder di " + global.nomeFile )();
								display@Monitor("Il jeer " + myInformation.address + " ha scaricato il file " + global.nomeFile);
								display@Monitor("Il jeer " + myInformation.address + " è diventato un seeder del file " + global.nomeFile)
							}
						} else {
							throw(UnsupportedOperation)
						}
					};
			scelte
}

define doMenu3{
		 	if(scelta == 3){ //cerca file da scaricare
					// PLUG-IN PER LA BARRA DI DOWNLOAD DEI FILE
				showAllFiles@Jolella()(files);
				println@Console( "\nEcco tutti i file presenti attualmente sulla rete ")();
				for(i=0, i<#files.x, i++) {
					println@Console( files.x[i] )()
				}
			};
			global.TiConfermoCheSonoUnTre=true
}

define printForPathUpdate{
  	println@Console( "Inserisci il path della cartella da condividere." )();
	println@Console("Se all'interno del path saranno presenti delle directory quest'ultime verranno compresse al fine di facilitarne la condivisione")();
	println@Console("Questa operazione può richiedere qualche secondo...")();
	global.TiConfermoCheSonoUnQuattro=true
}

define doMenu4 {
	exists@File(global.pathUpdate)(pathExists);
	if(pathExists){
		k=0;
		println@Console()();
		for (i=0, i<#global.forAddFile, i++) {
			removeFile@Jolella(global.forAddFile[i])(removed)
		};
		undef(global.forAddFile);
		println@Console("\nI file della vecchia cartella sono stati rimossi con successo. ")();
		//da qui faccio lo stesso lavoro di prima, listo i file e li aggiungo alla rete tranne cartelle e hidden file
		global.pathDirectoryToShare = global.pathUpdate;
		println@Console( "File resi disponibili: " )();
		reqFiles.directory=global.pathUpdate;
		println@Console( reqFiles.directory )();
		list@File(reqFiles)(resFiles);
		for( i=0 , i<#resFiles.result , i++){
			//controllo se ci sono cartella all'interno della directory e nel caso le zippo e il file compresso viene caricato nella rete
			checkIsDir=global.pathUpdate + "/" + resFiles.result[i];
			isDirectory@File( checkIsDir )( isDir );
			if(isDir){
				execRequest = "tar";
				with( execRequest ){
					.args[0] = "-zcvf";
					.args[1] = resFiles.result[i] + ".tar.gz";
					.args[2] = resFiles.result[i];
					.workingDirectory = global.pathUpdate;
		      	.stdOutConsoleEnable = false
				};
				exec@Exec( execRequest )( execResponse )
			}
		};
		k=0;
		list@File(reqFiles)(resFiles);
		//controllo se nella directory ci sono altre cartelle e nel caso non le carico perchè è presente il formato ".targz"
		//oppure i files iniziano con il ".", in quanto sono files del sistema operativo
		for( i=0 , i<#resFiles.result , i++){
		  	checkIsDir=scelta + "/" + resFiles.result[i];
		  	isDirectory@File( checkIsDir )( isDir );
		  	if(!isDir){
				reqStartsWith = resFiles.result[i];
				reqStartsWith.prefix = ".";
				startsWith@StringUtils(reqStartsWith)(resStartsWith);
				//non vogliamo file che inizino con il punto in quanto sono file dell'os
				if(!resStartsWith){
					global.forAddFile[k].id=myInformation.id;
					global.forAddFile[k].item=resFiles.result[i];
					println@Console( global.forAddFile[k].item )();
					addFile@Jolella( global.forAddFile[k] );
					k++
				}
		  	}
		}
	} else {
		println@Console( "Path Errato. Riprovare." )()
	};
	global.TiConfermoCheSonoUnQuattro=false;
	scelte
}

//elimina un file condiviso sulla rete, presente nella cartella specificata
define doMenu5 {
	removed=false;
	for ( i=0, i<#global.forAddFile, i++ ) {
	  	if(global.forAddFile[i].item==scelta){
	  		removeFile@Jolella(global.forAddFile[i])(removed);
	  		undef(global.forAddFile[i])
	  	}
	};
	println@Console( "File disponibili\n")();
	for ( k=0, k<#global.forAddFile, k++ ) {
	  	println@Console( global.forAddFile[k].item )()
	};
	if(removed) {
		println@Console("\n\tFile rimosso con successo!")()
	} else {
		println@Console("Non puoi rimuovere un file che non possiedi!")()
	};
	scelte;
	global.TiConfermoCheSonoUnCinque=false
}

define mainMenu {
	scope (doMainMenu){
		install ( ChoiceNotValid =>println@Console("Scelta non valida. ")() );
		if(scelta==0 && global.TiConfermoCheSonoUnTre==false && global.vaiPureAvanti==false && !global.TiConfermoCheSonoUnQuattro){
				global.online=false; //online: variabile che blocca tutte le altre scelte
				doMenu0
			}else if(scelta==1 && global.online==true && global.TiConfermoCheSonoUnTre==false && !global.TiConfermoCheSonoUnQuattro){
				doMenu1
			}else if(scelta==2 && global.online==true && global.TiConfermoCheSonoUnTre==false && !global.TiConfermoCheSonoUnQuattro){
				doMenu2;
				halt@Runtime()()
				//ci vuole qualcosa che lo blocca
			}else if(scelta==3 && global.online==true && global.TiConfermoCheSonoUnTre==false && !global.TiConfermoCheSonoUnQuattro){
				doMenu3;
				println@Console("\nInserisci il nome del file che stai cercando: ")()
			}else if(scelta==4 && global.online && !global.TiConfermoCheSonoUnTre && !global.TiConfermoCheSonoUnQuattro){
				printForPathUpdate
			}else if(global.TiConfermoCheSonoUnQuattro){ //cioè nell'esecuzione precedente scelta era uguale a 4 quindi ora scelta sarà il path da cambiare
				global.pathUpdate=scelta;
				doMenu4
			}else if(scelta==5 && global.online==true && global.TiConfermoCheSonoUnTre==false && !global.TiConfermoCheSonoUnQuattro){
				println@Console("Inserisci il nome del file da rimuovere")();
				global.TiConfermoCheSonoUnCinque=true
			}else if(global.TiConfermoCheSonoUnCinque==true){ //cioè nell'esecuzione precedente scelta era uguale a 5 quindi ora scelta sarà il nome del file da rimuovere
			doMenu5
			}else if(global.TiConfermoCheSonoUnTre==true){ //cioè se nel "ciclo" precedente ho premuto tre ora scelta sarà IL FILE da cercare
			global.nomeFile = scelta;
			global.checkFile = scelta;
			ricercaFile;
				//println@Console("global.vaiPureAvanti: "+global.vaiPureAvanti)();
			if(global.vaiPureAvanti==true){
					SceltaId; // da rinominare in "scaricaFile" o qualcosa del genere
					global.vaiPureAvanti=false
				};
				global.TiConfermoCheSonoUnTre=false //in questo modo posso compiere altre azioni al prossimo "ciclo"
			}else if(global.online==true && global.TiConfermoCheSonoUnTre==false && global.TiConfermoCheSonoUnQuattro==false){
		 		throw( ChoiceNotValid );
		 		scelte
			}else{
				println@Console("Premi 0 per riconnetterti")()
			}
	}
}

define doDownload {
	scope(downloadPart){
		install ( FileNotFound => println@Console("Impossibile trovare il file")());
		install ( UnsupportedOperation => println@Console("Impossibile convertire il file")() );
		file.filename = global.pathDirectoryToShare + "/" + fileScaricare.nome;
		// per mandare il file si usa il formato base64
		file.format = format = "base64";
		// lettura del file dalla memoria
		readFile@File(file)(dati);
		// generazione hash del file
	    md5@MessageDigest(dati)(hash);
	    if(hash instanceof JavaExceptionType){
			throw( UnsupportedOperation )
		} else {
			// vengono inviati sia il file che l'hash
			contenuto.hash = hash;
			string2split = dati;
			length@StringUtils( dati )( n ); //ritorna la il numero di caratteri del mio file ?
			parti = ( n / fileScaricare.tot ) + 1; //fileScaricare.tot= NUMERO DI UTENTI CHE HANNO IL FILE
			p = int ( parti );
	    	string2split.length = p;
			splitByLength@StringUtils( string2split )( array );
			contenuto.dati = array.result[fileScaricare.part];
			contenuto.part = fileScaricare.part
		}
	}
}

execution { concurrent }

main {

	[in(scelta)]{ mainMenu }

	// riceve una richiesta di download ed esegue l'upload del file (o del pezzo)
	[ download(fileScaricare)(contenuto) { doDownload } ] //in realtà quest'operazione dovrebbe chiamarsi UPLOAD

}
