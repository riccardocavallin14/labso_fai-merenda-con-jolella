/*
Fai Merenda con Jolella File Sharing P2P Netowork
Copyright (C) 2019 
<riccardo.cavallin@studio.unibo.it>
<emanuele.fazzini@studio.unibo.it>
<martino.simonetti@studio.unibo.it>
<michele.bonini2@studio.unibo.it>
<davide.cavallini4@studio.unibo.it>
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

type Content : void {
	.id : int
	.item : string
}

type JoinType : void {
	.id : int
	.address : string
}

type ListType: void {
	.id* : int
	.address* : string
}

type ShowType : void {
	.x* : string
}

type DownloadType : void {
	.dati : any
	.hash : string
	.part : int
}

type UploadType : void {
	.nome : string
	.part : int
	.tot : int
}

interface MyInterface {

    OneWay:
    display(string),
	leave (int),
	addFile(Content),
	disconnect(int),
	menu(string)


    RequestResponse:
    join(JoinType) (int),
    lookup(string) (ListType),
    removeFile(Content) (bool),
    showAllFiles(undefined) (ShowType),
	download(UploadType)(DownloadType)

}
